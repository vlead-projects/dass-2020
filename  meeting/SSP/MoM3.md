# Minutes of the Meeting
### Date: 29-01-2020 
### Time : 09:45 - 10:30 
### Topic: Third Client Meet

## Attendees: 
### Clients - Priya, Balamma, Niranjan
### Team Members - Vipul Chhabra, Aryamaan Jain, Lakshmi Madhuri, Shubhangi Dutta

Type | Description | Owner | Deadline
-----|-------------|-------|---------
T | Update teammate's information in Gitlab repository | Madhuri Yarava | 1 February 2020
T | Create Milestones | Vipul Chhabra | 1 February 2020
T | Push minutes of meetings and worklogs| Vipul Chhabra, Aryamaan Jain, Lakshmi Madhuri, Shubhangi Dutta | 1 February 2020
T | Create directory in Gitlab repository and and push documents | Vipul Chhabra, Aryamaan Jain, Lakshmi Madhuri, Shubhangi Dutta | 1 February 2020
T | Create SRS Document | Shubhangi Dutta, Aryamaan Jain | 1 February 2020
I | Give as many details as possible | - | -
D | Give individual ownership to issues | - | -
