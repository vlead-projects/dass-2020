# Minutes of the Meeting
### Date: 31-01-2020 
### Time : 09:45 - 10:30 
### Topic: Sprint 2 Team Meet

## Attendees: 

### Team Members - Vipul Chhabra, Aryamaan Jain, Lakshmi Madhuri, Shubhangi Dutta

Type | Description | Owner | Deadline
-----|-------------|-------|---------
T | Pushed minutes of meetings and worklogs| Vipul Chhabra, Aryamaan Jain, Lakshmi Madhuri, Shubhangi Dutta | 1 February 2020
T | Created SRS Document | Shubhangi Dutta, Aryamaan Jain | 1 February 2020
T | Project Plan Document | Vipul Chhabra, Aryamaan Jain, Lakshmi Madhuri, Shubhangi Dutta | 1 February 2020
D | Milestones to be created | Vipul Chhabra | 1 February 2020
D | Experiment wise sprint plan | - | -
D | Issues to be raised on Gitlab | - | -
