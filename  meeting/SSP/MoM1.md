# Minutes of the Meeting
### Date: 17-01-2020 
### Time : 09:45 - 10:30 
### Topic: First Client Meet

## Attendees: 
### Clients - Priya, Balamma
### Mentor - Abhinav Gupta
### Team Members - Vipul Chhabra, Aryamaan Jain, Lakshmi Madhuri, Shubhangi Dutta 


Type | Description | Owner | Deadline
-----|-------------|-------|---------
T | Convert entire codebase to JS | Vipul Chhabra, Aryamaan Jain, Lakshmi Madhuri, Shubhangi Dutta | April 2020
T | Create a new folder with the name DASS on GitHub. | Abhinav Gupta | January 2020
T | Maintain the uniform structure of every experiment. | Vipul Chhabra, Aryamaan Jain, Lakshmi Madhuri, Shubhangi Dutta | April 2020
T | If possible, automate the testing environment for the project | Vipul Chhabra, Aryamaan Jain, Lakshmi Madhuri, Shubhangi Dutta | April 2020
T | Make week-wise milestones | Vipul Chhabra, Aryamaan Jain, Lakshmi Madhuri, Shubhangi Dutta | 22 January 2020
T | If possible, improve the aesthetics | Vipul Chhabra, Aryamaan Jain, Lakshmi Madhuri, Shubhangi Dutta | April 2020
T | If possible, improve the text information | Vipul Chhabra, Aryamaan Jain, Lakshmi Madhuri, Shubhangi Dutta |
D | Every Wednesday there will be a weekly meeting from 10:30-11:30 AM. | - | -
D | Use names of the experiments, and not numbers, as identifiers | - | -
D | Use only open source libraries, such as D3 for graphics | - | -