# Minutes of the Meeting
### Date: 22-01-2020 
### Time : 09:45 - 10:30 
## Topic: Second Client Meet

## Attendees: 
### Clients - Priya, Balamma, Niranjan
### Mentor - Abhinav Gupta
### Team Members - Vipul Chhabra, Aryamaan Jain, Lakshmi Madhuri

Type | Description | Owner | Deadline
-----|-------------|-------|---------
T | Check if experiments can be converted or not | Vipul Chhabra, Aryamaan Jain, Lakshmi Madhuri, Shubhangi Dutta | 29 January 2020
T | Log all changes to be made as issues | Vipul Chhabra, Aryamaan Jain, Lakshmi Madhuri, Shubhangi Dutta | 29 January 2020
T | Adding phone numbers | Abhinav Gupta | -
T | Show one proof of concept | Vipul Chhabra, Aryamaan Jain, Lakshmi Madhuri, Shubhangi Dutta | 29 January 2020
T | Install Ubuntu 12 | Vipul Chhabra, Aryamaan Jain, Lakshmi Madhuri, Shubhangi Dutta | 29 January 2020
T | List libraries required | Vipul Chhabra, Aryamaan Jain, Lakshmi Madhuri, Shubhangi Dutta | 29 January 2020
T | If possible, improve the quiz | Vipul Chhabra, Aryamaan Jain, Lakshmi Madhuri, Shubhangi Dutta |
I | Communicate with mentors over Slack. | - | -
D | Github should have technical code | - | -
D | Gitlab should have progress made, such as weekly worklogs, MoMs. | - | -
D | Wednesday weekly meeting for 15 minutes per team: 5 minutes for status update, 5 minutes for what is going to be done, 5 minutes for any other. | - | -